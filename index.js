const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
const bodyParser = require('body-parser')

const books = require("./books.json").books.map(book => ({
  ...book, createdAt: new Date(book.createdAt), takenUntil: book.takenUntil ? new Date(takenUntil) : null  
}));

let pointer = books.reduce((acc, cur) => cur.id > acc ? cur.id : acc, books[0]).id + 1; 

express()
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: false }))
  .use(express.static(path.join(__dirname, 'public')))
  .set('views', path.join(__dirname, 'views'))
  .set('view engine', 'pug')
  .get('/books', (req, res) => res.json(books))
  .post('/add', (req, res) => {
    const {name, author, createdAt} = req.body;
    const id = pointer;
    pointer = pointer + 1;

    const isValid = !Number.isNaN(new Date(createdAt).getTime());

    console.log({name, author, createdAt})
    

    if (isValid) {
      books.push({id, name, author, createdAt: new Date(createdAt), takerName: null, takenUntil: null})
    } 

    console.log(books)

    res.redirect("back");    
  })
  .post('/change', (req, res) => {
    const {name, author, createdAt, id} = req.body;

    const isValid = !Number.isNaN(new Date(createdAt).getTime());

    console.log({name, author, createdAt, id})

    if (isValid) {      
      const book = books.find(book => book.id === Number(id));
      book.name = name
      book.author = author
      book.createdAt = new Date(createdAt)
    }
    

    res.redirect("back");
  })
  .post('/take', (req, res) => {
    const {name, id} = req.body; 
    
    const book = books.find(book => book.id === Number(id));
    book.takerName = name;
    book.takenUntil = new Date(new Date().getTime() + 86400000 * 10);

    res.redirect("back");
  })
  .post('/return', (req, res) => {
    const {name, id} = req.body; 
    
    const book = books.find(book => book.id === Number(id));
    if (book.takerName === name) {
      book.takerName = null;
      book.takenUntil = null;
    } 
    
    res.redirect("back");
  })
  .get('/existed', (req, res) => {
    res.json(books.filter(book => !book.takenUntil))
  })
  .get('/taken_until', (req, res) => {
    const {date} = req.query; 
    
    console.log(date, new Date(date).getTime());
    console.log(books[0], new Date(books[0].createdAt).getTime());

    res.json(books.filter(book => book.takenUntil.toDateString() === new Date(date).toDateString()).map(item => ({...item, createdAt: item.createdAt.toDateString(), takerName: item.takerName ? item.takerName : "Exists", takenUntil: item.takenUntil ? item.takenUntil.toDateString() : "Exists"})))
  })
  .get('/:id', (req, res) => {
    const {id} = req.params;

    const book = books.find(book => book.id === Number(id));

    if(!book) {
      res.redirect("back")
    } else {
      res.render('pages/book', { title: 'Book', message: 'Hello there!', book: ({...book, createdAt: book.createdAt.toDateString(), takerName: book.takerName ? book.takerName : "Exists", takenUntil: book.takenUntil ? book.takenUntil.toDateString() : "Exists"})})
    }

  })
  .get('/', (req, res) => res.render('pages/index', { title: 'Books', books: books.map(item => ({...item, createdAt: item.createdAt.toDateString(), takerName: item.takerName ? item.takerName : "Exists", takenUntil: item.takenUntil ? item.takenUntil.toDateString() : "Exists"}))}))
  
  .listen(PORT, () => console.log(`Listening on ${ PORT }`))