const checkIsUserExists = () => !!($.cookie("name"))
const setUser = () => {
    ($.cookie("name", prompt("Set user name")));
}
const getUser = () => $.cookie("name")

const rebuildButtons = () => {
    const take_button = $("#take_button");
    const return_button = $("#return_button");
    const takerName = $("#takerName").html();

    console.log(takerName)

    if (takerName !== "Exists") {
        take_button.hide()
    }

    if (takerName !== getUser()) {
        return_button.hide()
    }

    if(take_button && return_button && takerName) {
        take_button[0].name.value = getUser()
        return_button[0].name.value = getUser()
    }
    
    return
}

const rebuildBooks = (books) => {    
    $("#container").html(books.map(book => `
        <div class="book">    
            <div class="book-line">${book.id}</div>
            <div class="book-line">${book.name}</div>
            <div class="book-line">${book.author}</div>
            <div class="book-line">${book.createdAt}</div>
            <div class="book-line">${book.takerName}</div>
            <div class="book-line">${book.takenUntil}</div>
        </div>
    `).join(""));
}

const getBooks = async () => {
    const books = (await fetch("/books")).json();
    rebuildBooks(await books);

    return
}
const getExistedBooks = async (e) => {
    e.preventDefault();
    
    const books = (await fetch("/existed")).json();
    rebuildBooks(await books)

    return
}
const getByTakenUntil = async (e) => {
    e.preventDefault();

    console.log(e.target.date.value)

    const date = e.target.date.value;
    const books = (await fetch(`/taken_until?date=${date}`)).json()

    rebuildBooks(await books)

    return
}

const resetUser = (e) => {
    e.preventDefault()
    setUser()
}

const setupForms = () => {
    const existedForm = $("#existed");
    const takenUntilForm = $("#taken_until");
    const allForm = $("#all");
    const resetUserForm = $("#reset_user");

    if (existedForm && takenUntilForm) {
        existedForm.submit(getExistedBooks)
        takenUntilForm.submit(getByTakenUntil)
        allForm.submit(getBooks)
        resetUserForm.submit(resetUser)
    }
}

$( document ).ready(function() {
    if(!checkIsUserExists()) {
        setUser()
    }

    rebuildButtons()
    setupForms()
});